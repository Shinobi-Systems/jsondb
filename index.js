const fs = require('fs')
module.exports = (filePath,options) => {
    var jsonData = {}
    options = options || {
        debug: true
    }
    const sessionErrors = []
    const debugLog = (...errs) => {
        if(options.debug === true)console.log(...errs)
    }
    try{
        jsonData = JSON.parse(fs.readFileSync(filePath,'utf8'))
    }catch(err){
        var parentFolder = filePath.split('/')
        parentFolder = parentFolder.splice(0,parentFolder.length - 2 ).join('/')
        fs.mkdirSync(parentFolder, { recursive: true });
        fs.writeFileSync(filePath,'{}')
        sessionErrors.push(err)
        debugLog(err)
    }
    const getAll = () => {
        return Object.assign(jsonData,{})
    }
    const getErrors = () => {
        return sessionErrors
    }
    const writeMemoryData = async () => {
        return await fs.promises.writeFile(filePath,JSON.stringify(jsonData))
    }
    const setToJson = async (key, value) => {
        try{
            jsonData[key] = value
            await writeMemoryData()
            return true
        }catch(err){
            sessionErrors.push(err)
            debugLog(err)
            return false
        }
    }
    const getFromJson = async (key) => {
        return jsonData[key]
    }
    const removeFromJson = async (key) => {
        try{
            delete(jsonData[key])
            await writeMemoryData()
            return true
        }catch(err){
            sessionErrors.push(err)
            debugLog(err)
            return false
        }
    }
    const destroyJson = async (key) => {
        try{
            jsonData = {}
            await writeMemoryData()
            return true
        }catch(err){
            sessionErrors.push(err)
            debugLog(err)
            return false
        }
    }
    return {
        getAll: getAll,
        setItem: setToJson,
        getItem: getFromJson,
        removeItem: removeFromJson,
        clear: destroyJson,
        getErrors: getErrors,
    }
}
